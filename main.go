package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Dice struct {
	sides int
}

func (d *Dice) Roll() int {
	return rand.Intn(d.sides) + 1
}

type Player struct {
	name  string
	score int
}

func main() {
	rand.Seed(time.Now().UnixNano())
	fmt.Println("How many players are there?")
	var numPlayers int
	fmt.Scanln(&numPlayers)

	players := make([]Player, numPlayers)

	for i := 0; i < numPlayers; i++ {
		fmt.Printf("Enter the name of player %d: ", i+1)
		var name string
		fmt.Scanln(&name)
		players[i] = Player{name: name}
	}

	die1 := Dice{sides: 6}
	die2 := Dice{sides: 6}

	for i, player := range players {
		fmt.Printf("%s, roll the dice by pressing ...", player.name)
		fmt.Scanln()

		roll1 := die1.Roll()
		roll2 := die2.Roll()
		total := roll1 + roll2

		fmt.Printf("%s rolled a %d and a %d, for a total of %d.\n", player.name, roll1, roll2, total)

		switch total {
		case 7, 11:
			fmt.Printf("%s wins!\n", player.name)
			player.score++
		case 2, 3, 12:
			fmt.Printf("%s loses!\n", player.name)
			player.score--
		default:
			fmt.Printf("%s rolls again!\n", player.name)
		}

		players[i] = player
	}

	for _, player := range players {
		fmt.Printf("%s scored %d\n", player.name, player.score)
	}
}
